<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>IPCX : IPCHEX - IP Address Checker</title>
	<link rel="stylesheet" type="text/css" href="css/primer.css" />
</head>

<body>
	<div id="container">
		<header>
			<h1> IPCX : IPCHEX </h1>
			<h3> Your Most Trusted!! IP Address Checker </h3>
			<h3> No Cookies, No Trackers, No Javascript required. </h3>
		</header>
	
		<main>
			<p>Remote IP Address : <%=request.getRemoteAddr() %> </p>
			<p>Remote Host : <%=request.getRemoteHost() %> </p>
			<p>Remote Port : <%=request.getRemotePort() %> </p>
			<br/>
			<p>Local IP Address : <%=request.getLocalAddr() %> </p>
			<p>Local Host : <%=request.getLocalName() %> </p>
			<p>Local Port : <%=request.getLocalPort() %> </p>
		</main>
		
		<footer>
			&copy; 2016 - 2021
		</footer>
		
	</div>
</body>
</html>
